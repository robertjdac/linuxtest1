#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

void* tester(void* arg)
{
	while (1)
	{
		int i = 1;

		i += 5;
	}

	printf("done\n");
}

int main()
{
	printf("Hello!\n");

	pthread_t t;

	pthread_create(&t, NULL, tester, NULL);

	pthread_join(t, NULL);

	return 0;
}